<?php

namespace Domain\Attribute\Repositories\Contracts;

use Domain\Attribute\Models\Attribute;
use Domain\Product\Models\Product;
use Illuminate\Database\Eloquent\Collection;

interface AttributeRepositoryInterface
{
    public function getForProduct(Product $product): Collection;

    public function create(array $attributes): Attribute;

    public function update(Attribute $attribute, array $attributes): Attribute;
}
