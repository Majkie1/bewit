<?php

namespace Domain\Attribute\Repositories;

use Domain\Attribute\Models\Attribute;
use Domain\Attribute\Repositories\Contracts\AttributeRepositoryInterface;
use Domain\Product\Models\Product;
use Illuminate\Database\Eloquent\Collection;

class AttributeRepository implements AttributeRepositoryInterface
{
    public function getForProduct(Product $product): Collection
    {
        return $product->attributes()->get();
    }

    public function create(array $attributes): Attribute
    {
        $attribute = new Attribute();

        $attribute->fill($attributes);
        $attribute->save();

        return $attribute;
    }

    public function update(Attribute $attribute, array $attributes): Attribute
    {
        $attribute->fill($attributes);
        $attribute->save();

        return $attribute;
    }
}
