<?php

namespace Domain\Product\Repositories\Contracts;

use Domain\Product\Models\Product;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepositoryInterface
{
    public function search(?array $attributes): Collection;

    public function create(array $attributes): Product;

    public function update(Product $product, array $attributes): Product;
}
