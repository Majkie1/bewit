<?php

declare(strict_types=1);

namespace Domain\Product\Repositories;

use Domain\Product\Models\Product;
use Domain\Product\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository implements ProductRepositoryInterface
{
    public function search(?array $attributes): Collection
    {
        $query = Product::query()
            ->with('attributes');

        if ($attributes === null) {
            return $query->get();
        }

        foreach ($attributes as $attribute) {
            $query->whereHas('attributes', function (Builder $query) use ($attribute) {
                $query->when($attribute['name'] === 'like',
                    function (Builder $query) use ($attribute) {
                        $query->where('name', '=', $attribute['name'])
                            ->where('name', 'like', '%' . $attribute['value'] . '%');
                    },
                    function (Builder $query) use ($attribute) {
                        $query->where('name', '=', $attribute['name'])
                            ->where(
                                'value',
                                $attribute['operator'],
                                is_numeric($attribute['value'])
                                    ? (int) $attribute['value']
                                    : $attribute['value'],
                            );
                    },
                );
            });
        }

        return $query->get();
    }

    public function create(array $attributes): Product
    {
        $product = new Product();

        $product->fill($attributes);
        $product->save();

        return $product;
    }

    public function update(Product $product, array $attributes): Product
    {
        $product->fill($attributes);
        $product->save();

        return $product;
    }
}
