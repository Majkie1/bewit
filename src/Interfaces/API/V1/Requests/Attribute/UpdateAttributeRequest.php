<?php

namespace Interfaces\API\V1\Requests\Attribute;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAttributeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['string', 'max:255'],
            'value' => ['string', 'max:255'],
        ];
    }
}
