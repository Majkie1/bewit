<?php

declare(strict_types=1);

namespace Interfaces\API\V1\Controllers\Attribute;

use App\Http\Controllers\ApiController;
use Domain\Attribute\Models\Attribute;
use Domain\Attribute\Repositories\Contracts\AttributeRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Interfaces\API\V1\Requests\Attribute\UpdateAttributeRequest;

class UpdateAttribute extends ApiController
{
    public function __construct(
        private readonly AttributeRepositoryInterface $attributeRepository,
    )
    {
    }

    public function __invoke(UpdateAttributeRequest $request, Attribute $attribute): JsonResponse
    {
        $this->attributeRepository->update($attribute, $request->validated());

        return $this->successResponse(message: 'Attribute updated successfully');
    }
}
