<?php

namespace Interfaces\API\V1\Controllers\Attribute;

use App\Http\Controllers\ApiController;
use Domain\Attribute\Models\Attribute;
use Illuminate\Http\JsonResponse;

class DeleteAttribute extends ApiController
{
    public function __invoke(Attribute $attribute): JsonResponse
    {
        $attribute->delete();

        return $this->successResponse(message: 'Attribute deleted successfully.');
    }
}
