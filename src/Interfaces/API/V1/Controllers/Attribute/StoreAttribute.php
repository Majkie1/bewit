<?php

namespace Interfaces\API\V1\Controllers\Attribute;

use App\Http\Controllers\ApiController;
use Domain\Attribute\Repositories\Contracts\AttributeRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Interfaces\API\V1\Requests\Attribute\StoreAttributeRequest;

class StoreAttribute extends ApiController
{
    public function __construct(
        private readonly AttributeRepositoryInterface $attributeRepository,
    )
    {
    }

    public function __invoke(StoreAttributeRequest $request): JsonResponse
    {
        $this->attributeRepository->create($request->validated());

        return $this->successResponse(message: 'Attribute created successfully.', code: 201);
    }
}
