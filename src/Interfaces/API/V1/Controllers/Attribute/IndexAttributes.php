<?php

namespace Interfaces\API\V1\Controllers\Attribute;

use App\Http\Controllers\ApiController;
use Domain\Attribute\Repositories\Contracts\AttributeRepositoryInterface;
use Domain\Product\Models\Product;
use Interfaces\API\V1\Resources\Attribute\AttributeCollection;

class IndexAttributes extends ApiController
{
    public function __construct(
        private readonly AttributeRepositoryInterface $attributeRepository,
    )
    {
    }

    public function __invoke(Product $product): AttributeCollection
    {
        $attributes = $this->attributeRepository->getForProduct($product);

        return new AttributeCollection($attributes);
    }
}
