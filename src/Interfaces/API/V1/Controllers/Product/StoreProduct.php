<?php

namespace Interfaces\API\V1\Controllers\Product;

use App\Http\Controllers\ApiController;
use Domain\Product\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Interfaces\API\V1\Requests\Product\StoreProductRequest;

class StoreProduct extends ApiController
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
    )
    {
    }

    public function __invoke(StoreProductRequest $request): JsonResponse
    {
        $this->productRepository->create($request->validated());

        return $this->successResponse(message: 'Product created successfully', code: 201);
    }
}
