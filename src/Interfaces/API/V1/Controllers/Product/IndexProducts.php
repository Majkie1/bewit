<?php

namespace Interfaces\API\V1\Controllers\Product;

use App\Http\Controllers\ApiController;
use Domain\Product\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Interfaces\API\V1\Resources\Product\ProductCollection;

class IndexProducts extends ApiController
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository
    )
    {
    }

    public function __invoke(Request $request): ProductCollection
    {
        $products = $this->productRepository->search($request->query('attributes'));

        return new ProductCollection($products);
    }
}
