<?php

namespace Interfaces\API\V1\Controllers\Product;

use App\Http\Controllers\ApiController;
use Domain\Product\Models\Product;
use Illuminate\Http\JsonResponse;

class DeleteProduct extends ApiController
{
    public function __invoke(Product $product): JsonResponse
    {
        $product->delete();

        return $this->successResponse(message: 'Product deleted successfully.');
    }
}
