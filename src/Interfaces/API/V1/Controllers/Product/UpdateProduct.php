<?php

declare(strict_types=1);

namespace Interfaces\API\V1\Controllers\Product;

use App\Http\Controllers\ApiController;
use Domain\Product\Models\Product;
use Domain\Product\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Interfaces\API\V1\Requests\Product\UpdateProductRequest;

class UpdateProduct extends ApiController
{
    public function __construct(
        private readonly ProductRepositoryInterface $productRepository,
    )
    {
    }

    public function __invoke(UpdateProductRequest $request, Product $product): JsonResponse
    {
        $this->productRepository->update($product, $request->validated());

        return $this->successResponse(message: 'Product updated successfully');
    }
}
