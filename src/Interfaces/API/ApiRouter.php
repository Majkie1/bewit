<?php

namespace Interfaces\API;

use App\Http\Routers\Router;
use Interfaces\API\V1\Controllers\Attribute\DeleteAttribute;
use Interfaces\API\V1\Controllers\Attribute\IndexAttributes;
use Interfaces\API\V1\Controllers\Attribute\StoreAttribute;
use Interfaces\API\V1\Controllers\Attribute\UpdateAttribute;
use Interfaces\API\V1\Controllers\Product\DeleteProduct;
use Interfaces\API\V1\Controllers\Product\IndexProducts;
use Interfaces\API\V1\Controllers\Product\StoreProduct;
use Interfaces\API\V1\Controllers\Product\UpdateProduct;

class ApiRouter extends Router
{

    public function map(): void
    {
        $this->router->group($this->apiRouterAttributes(), function () {

            // V1
            $this->router->group($this->v1RouterAttributes(), function () {

                // Products
                $this->router->get('/products', IndexProducts::class)->name(ApiRoutes::INDEX_PRODUCTS->value);
                $this->router->post('/products', StoreProduct::class)->name(ApiRoutes::STORE_PRODUCT->value);
                $this->router->patch('/products/{id}', UpdateProduct::class)->name(ApiRoutes::UPDATE_PRODUCT->value);
                $this->router->delete('/products/{id}', DeleteProduct::class)->name(ApiRoutes::DELETE_PRODUCT->value);

                // Attributes
                $this->router->get('/attributes/{product}', IndexAttributes::class)->name(ApiRoutes::INDEX_ATTRIBUTES->value);
                $this->router->post('/attributes/create', StoreAttribute::class)->name(ApiRoutes::STORE_ATTRIBUTE->value);
                $this->router->patch('/attributes/{attribute}', UpdateAttribute::class)->name(ApiRoutes::UPDATE_ATTRIBUTE->value);
                $this->router->delete('/attributes/{attribute}', DeleteAttribute::class)->name(ApiRoutes::DELETE_ATTRIBUTE->value);

            });

        });
    }

    private function apiRouterAttributes(): array
    {
        return [
            'prefix' => 'api',
            'middleware' => [
                'api',
            ],
        ];
    }

    private function v1RouterAttributes(): array
    {
        return [
            'prefix' => 'v1',
        ];
    }
}
