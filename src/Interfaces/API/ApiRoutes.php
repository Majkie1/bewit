<?php

namespace Interfaces\API;

enum ApiRoutes: string
{
    // Products
    case INDEX_PRODUCTS = 'index_products';
    case STORE_PRODUCT = 'store_product';
    case UPDATE_PRODUCT = 'update_product';
    case DELETE_PRODUCT = 'delete_product';

    // Attributes
    case INDEX_ATTRIBUTES = 'api.v1.attributes.index';
    case STORE_ATTRIBUTE = 'api.v1.attributes.store';
    case UPDATE_ATTRIBUTE = 'api.v1.attributes.update';
    case DELETE_ATTRIBUTE = 'api.v1.attributes.delete';
}
