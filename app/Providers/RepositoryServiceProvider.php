<?php

namespace App\Providers;

use Domain\Attribute\Repositories\AttributeRepository;
use Domain\Attribute\Repositories\Contracts\AttributeRepositoryInterface;
use Domain\Product\Repositories\Contracts\ProductRepositoryInterface;
use Domain\Product\Repositories\ProductRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    private array $repositories = [
        AttributeRepositoryInterface::class => AttributeRepository::class,
        ProductRepositoryInterface::class => ProductRepository::class,
    ];

    public function register(): void
    {
        foreach ($this->repositories as $interface => $repository) {
            $this->app->bind($interface, $repository);
        }
    }
}
