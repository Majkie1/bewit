<?php

namespace App\Http\Controllers;

use App\Concerns\ApiResponseTrait;

abstract class ApiController extends Controller
{
    use ApiResponseTrait;
}
