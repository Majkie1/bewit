<?php

declare(strict_types=1);

namespace App\Http\Routers;

use Illuminate\Routing\Router as IlluminateRouter;

abstract class Router
{
    public function __construct(protected IlluminateRouter $router)
    {
        $this->map();
    }

    abstract public function map(): void;
}
