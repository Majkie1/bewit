<?php

namespace Database\Seeders;

use Domain\Product\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    public function run(): void
    {
        Product::factory()
            ->count(10)
            ->create();
    }
}
