<?php

namespace Database\Seeders;

use Domain\Attribute\Models\Attribute;
use Domain\Product\Models\Product;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    public function run()
    {
        foreach (Product::cursor() as $product) {
            Attribute::factory()
                ->state([
                    'product_id' => $product->id,
                ])
                ->count(2)
                ->create();

            Attribute::factory()
                ->state([
                    'product_id' => $product->id,
                ])
                ->valueAsNumber()
                ->count(3)
                ->create();
        }
    }
}
