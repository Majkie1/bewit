<?php

namespace Database\Factories;

use Domain\Attribute\Models\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttributeFactory extends Factory
{
    protected $model = Attribute::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->word,
            'value' => $this->faker->word,
        ];
    }

    public function valueAsNumber(): self
    {
        return $this->state(function () {
            return [
                'name' => $this->faker->randomElement(['price', 'weight', 'length']),
                'value' => $this->faker->numberBetween(1, 10000),
            ];
        });
    }
}
